package com.example.administrator.android_a1607_okhttp.Thread;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.administrator.android_a1607_okhttp.Bean.Bean;
import com.example.administrator.android_a1607_okhttp.Fragment.RemenFragmen;
import com.example.administrator.android_a1607_okhttp.HttpUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Administrator on 2016/8/27.
 */
public class LoadJsonTask extends AsyncTask<String, Void, List<Bean>> {




    public interface OnGetJsonResultListener {
        void onGetJsonResult(List<Bean> result);
    }

    private OnGetJsonResultListener mListener;

    public LoadJsonTask(OnGetJsonResultListener listener){
        mListener=listener;
    }

    protected List<Bean> doInBackground(String... params) {
        try {
            byte[] data = HttpUtils.loadData(params[0]);

            ArrayList<Bean> list = new ArrayList<>();
            String json = new String(data);
            JSONArray jsonArray=new JSONArray(json);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String recommend_caption = jsonObject.getString("recommend_caption");
                String recommend_cover_pic = jsonObject.getString("recommend_cover_pic");
                String caption = jsonObject.getString("caption");
                String video = jsonObject.getString("video");
                String likes_count = jsonObject.getString("likes_count");
                Bean bean=new Bean(recommend_caption,recommend_cover_pic,caption,video,likes_count);
                list.add(bean);
            }
            return list;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<Bean> result) {
        super.onPostExecute(result);
        if (mListener != null && result != null) {

            mListener.onGetJsonResult(result);
        }
    }

}
