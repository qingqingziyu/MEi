package com.example.administrator.android_a1607_okhttp.Fragment;


import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.administrator.android_a1607_okhttp.Adapter.MyAdapter;
import com.example.administrator.android_a1607_okhttp.Bean.Bean;
import com.example.administrator.android_a1607_okhttp.R;
import com.example.administrator.android_a1607_okhttp.Thread.LoadJsonTask;
import com.example.administrator.android_a1607_okhttp.Url.HttpUrl;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class RemenFragmen extends Fragment implements LoadJsonTask.OnGetJsonResultListener{


    private RecyclerView recycler;
    private List<Bean> data;
    private MyAdapter adapter;

    public RemenFragmen() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_remen, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new LoadJsonTask(this).execute(HttpUrl.JSON_REMEN);
        //初始化
        recycler = (RecyclerView) view.findViewById(R.id.recycler);
        initAdapter();//适配器
        initlayout();//布局管理器
    }


    //适配器
    private void initAdapter() {
        adapter = new MyAdapter(data);
        recycler.setAdapter(adapter);
    }

    //布局管理器
    private void initlayout() {
        StaggeredGridLayoutManager manager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        //设置布局管理器
        recycler.setLayoutManager(manager);
//        添加修饰
        recycler.addItemDecoration(new MyDecoration(10));
    }

    @Override
    public void onGetJsonResult(List<Bean> result) {
        data=result;
    }


    //Decoration装饰，美化
    private class MyDecoration extends RecyclerView.ItemDecoration {
        private int moffset;

        public MyDecoration(int offset) {
            this.moffset=offset;
        }
        //用来获取每个条目的偏移量
        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            super.getItemOffsets(outRect, view, parent, state);
            outRect.left=moffset;
            outRect.bottom=5;
        }
    }

}
