package com.example.administrator.android_a1607_okhttp;

import android.os.Environment;

import java.io.File;

/**
 * 类描述:
 * 创建人:一一哥
 * 创建时间:16/9/12 14:46
 * 备注:
 */
public class MyConfigs {

    public static final String PATH = "http://10.0.162.92:8080/MyService/data2.json";

    //磁盘存储路径
    public static final String DISK_CACHE_DIR = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "1607cache";

    //json的存储路径
    public static final String JSON_DIR = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "1607cache" + File.separator;

    //最低存储空间
    public static final long DISK_MAX_SIZE = 5 * 1024 * 1024;
}
