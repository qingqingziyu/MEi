package com.example.administrator.android_a1607_okhttp;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;
import java.util.List;

/**
 * Created by Administrator on 2016/9/19 0019.
 */
public class MyPagerAdapter extends FragmentPagerAdapter {
    private List<Fragment> mList;

    public MyPagerAdapter(FragmentManager fm, List<Fragment> list) {
        super(fm);
        this.mList=list;
    }



    @Override
    public Fragment getItem(int position) {
        return mList.get(position);
    }

    @Override
    public int getCount() {
        return mList.size();
    }
}
