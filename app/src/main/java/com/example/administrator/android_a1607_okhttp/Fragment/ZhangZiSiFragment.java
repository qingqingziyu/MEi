package com.example.administrator.android_a1607_okhttp.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.administrator.android_a1607_okhttp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ZhangZiSiFragment extends Fragment {


    public ZhangZiSiFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_zhang_zi_si, container, false);
    }

}
