package com.example.administrator.android_a1607_okhttp;


import com.example.administrator.android_a1607_okhttp.Bean.Bean;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrator
 *         网络访问工具类
 */
public class HttpUtils {

    private static ByteArrayOutputStream baos;
    private static String name=null;
    public static byte[] loadData(String path) throws IOException {
        URL url = new URL(path);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setConnectTimeout(5000);
        conn.setDoInput(true);
        if (conn.getResponseCode() == 200) {
            InputStream is = conn.getInputStream();
            baos = new ByteArrayOutputStream();
            int len = 0;
            byte[] buffer = new byte[1024];
            while ((len = is.read(buffer)) != -1) {
                baos.write(buffer, 0, len);
            }
            return baos.toByteArray();
        }
        return null;
    }

    public static List<Bean> getJsonRes(String path) throws IOException, JSONException {

        List<Bean> list = new ArrayList<Bean>();
        JSONArray jsonArray = new JSONArray(path);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String recommend_caption = jsonObject.getString("recommend_caption");
            String recommend_cover_pic = jsonObject.getString("recommend_cover_pic");
            String caption = jsonObject.getString("caption");
            String video = jsonObject.getString("video");
            String likes_count = jsonObject.getString("likes_count");
            Bean bean = new Bean(recommend_caption, recommend_cover_pic, caption, video, likes_count);
            list.add(bean);
        }
        return list;
    }

}
