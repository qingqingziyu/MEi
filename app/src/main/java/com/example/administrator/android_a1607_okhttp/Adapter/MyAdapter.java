package com.example.administrator.android_a1607_okhttp.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.android_a1607_okhttp.Bean.Bean;
import com.example.administrator.android_a1607_okhttp.R;

import java.util.List;

/**
 * Created by Administrator on 2016/9/21 0021.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyHolder> {

//    定义数据源
    List<Bean> mList;
    //构造函数传值
    public MyAdapter(List<Bean> list){
        this.mList=list;
    }


    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
    //绑定数据
        Bean bean = mList.get(position);

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    static class MyHolder extends RecyclerView.ViewHolder {
        ImageView logo;
        TextView title,likeCount;

        public MyHolder(View itemView) {
            super(itemView);
            logo=(ImageView) itemView.findViewById(R.id.image_logo);
            title= (TextView) itemView.findViewById(R.id.tv_title);
            likeCount= (TextView) itemView.findViewById(R.id.tv_like);
        }
    }
}
